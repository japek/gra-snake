#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMessageBox>
#include <QMainWindow>
#include <QByteArray>
#include <QSettings>
#include <QDir>
#include "canvas.h"
#include "smallcipher.h"

QT_BEGIN_NAMESPACE
namespace Ui
{
    class MainWindow;
}
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    QSettings *settings;
    Ui::MainWindow *ui;
    Canvas *canvas;
    SmallCipher *scipher;

    void changeEvent(QEvent *) override;

    int loadHighScore();
    int loadAllEatApple();
    int loadLosers();
    void saveHighScore(int score);

private slots:

    void showRules();
    void showControls();
    void setHard();
    void setNormal();
    void setAuto();
    void setPenetration();
    void setBelching();
    void showHighScore();
    void showAllEatApple();
    void showLosers();
    void checkScore(int score);
    void saveAllEatApple(int score);
    void saveLosers();
};
#endif // MAINWINDOW_H
