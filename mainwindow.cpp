#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    canvas = new Canvas(this);
    ui->layout->addWidget(canvas);

    ui->actionNormal->setChecked(true);
    ui->actionNormal->setEnabled(false);

    canvas->setFocus();

    setMinimumSize(canvas->game->fieldWidth * 15, canvas->game->fieldHeight * 15);

    connect(ui->actionRules, SIGNAL(triggered()), this, SLOT(showRules()));

    connect(ui->actionControls, SIGNAL(triggered()), this, SLOT(showControls()));

    connect(ui->actionHigh_score, SIGNAL(triggered()), this, SLOT(showHighScore()));
    connect(ui->actionGetAllEatApple, SIGNAL(triggered()), this, SLOT(showAllEatApple()));
    connect(ui->actionLosers, SIGNAL(triggered()), this, SLOT(showLosers()));
    connect(canvas, SIGNAL(processScore(int)), this, SLOT(checkScore(int)));
    connect(canvas, SIGNAL(processScore(int)), this, SLOT(saveAllEatApple(int)));
    connect(canvas, SIGNAL(processDeath()), this, SLOT(saveLosers()));

    connect(ui->actionNormal, SIGNAL(triggered()), this, SLOT(setNormal()));
    connect(ui->actionHard, SIGNAL(triggered()), this, SLOT(setHard()));
    connect(ui->actionAuto, SIGNAL(triggered()), this, SLOT(setAuto()));
    connect(ui->actionPenetration, SIGNAL(triggered()), this, SLOT(setPenetration()));
    connect(ui->actionBelching, SIGNAL(triggered()), this, SLOT(setBelching()));

    scipher = new SmallCipher("Zmień kierunek węża za pomocą strzałek. Wstrzymaj / kontynuuj używając spacji.%%%%%%%%%%%%~");

    settings = new QSettings(QDir::currentPath() + "/settings.ini", QSettings::IniFormat);
}

MainWindow::~MainWindow()
{
    delete settings; // Destructor must write all unsaved changes
    delete canvas;
    delete ui;
    delete scipher;
}

void MainWindow::changeEvent(QEvent *event)
{
    if (event->type() == QEvent::LanguageChange)
    {
        event->accept();
        ui->retranslateUi(this);
    }
    else
    {
        event->ignore();
    }
}

void MainWindow::showRules()
{
    QMessageBox::information(this, tr("Zasady snake"),
                             tr("Opis gry!!!"));
}

void MainWindow::showControls()
{
    QMessageBox::information(this, tr("Snake sterowanie"),
                             tr("Zmień kierunek węża za pomocą strzałek. Wstrzymaj / kontynuuj używając spacji."));
}

void MainWindow::setHard()
{
    if (ui->actionHard->isChecked())
    {
        ui->actionHard->setEnabled(false);
    }

    ui->actionNormal->setChecked(false);
    ui->actionAuto->setChecked(false);
    ui->actionNormal->setEnabled(true);
    ui->actionAuto->setEnabled(true);

    canvas->setGameModeHard();
}

void MainWindow::setNormal()
{
    if (ui->actionNormal->isChecked())
    {
        ui->actionNormal->setEnabled(false);
    }

    ui->actionHard->setChecked(false);
    ui->actionAuto->setChecked(false);
    ui->actionHard->setEnabled(true);
    ui->actionAuto->setEnabled(true);

    canvas->setGameModeNormal();
}

void MainWindow::setAuto()
{
    if (ui->actionAuto->isChecked())
    {
        ui->actionAuto->setEnabled(false);
    }

    ui->actionHard->setChecked(false);
    ui->actionNormal->setChecked(false);
    ui->actionHard->setEnabled(true);
    ui->actionNormal->setEnabled(true);

    canvas->setGameModeAuto();
}

void MainWindow::setPenetration()
{
    if (!ui->actionPenetration->isChecked())
    {
        canvas->setPenetration(false);
    }
    else
    {
        ui->actionBelching->setChecked(false);
        canvas->setPenetration(true);
    }
}

void MainWindow::setBelching()
{
    if (!ui->actionBelching->isChecked())
    {
        canvas->setBelching(false);
    }
    else
    {
        ui->actionPenetration->setChecked(false);
        canvas->setBelching(true);
    }
}

void MainWindow::showHighScore()
{
    int hs = loadHighScore();

    QMessageBox::information(this, tr("Najlepszy wynik"),
                             tr("Twój najlepszy wynik to: %1").arg(hs));
}

void MainWindow::showAllEatApple()
{
    int apple = loadAllEatApple();

    QMessageBox::information(this, tr("Zjedzona jabłka"),
                             tr("Ilość zjedzonych jabłek to: %1").arg(apple));
}

void MainWindow::showLosers()
{
    int losers = loadLosers();

    QMessageBox::information(this, tr("Przegrane"),
                             tr("Ilość twoich przegranych: %1").arg(losers));
}

void MainWindow::saveAllEatApple(int score)
{
    int settingsApple = loadAllEatApple();
    settingsApple = settingsApple + score;

    QByteArray cipher = scipher->encrypt(scipher->intToBArray(settingsApple));
    settings->beginGroup("statistic");
    settings->setValue("apple", QString(cipher.toBase64()));
    settings->endGroup();
}

void MainWindow::saveLosers()
{
    int losers = loadLosers();
    losers++;

    QByteArray cipher = scipher->encrypt(scipher->intToBArray(losers));
    settings->beginGroup("statistic");
    settings->setValue("losers", QString(cipher.toBase64()));
    settings->endGroup();
}

void MainWindow::checkScore(int score)
{
    int highScore = loadHighScore();

    if (score > highScore)
    {
        saveHighScore(score);
    }
}

int MainWindow::loadHighScore()
{
    QString cipher_base64;

    settings->beginGroup("statistic");

    if (settings->contains("hscore"))
    {
        cipher_base64 = settings->value("hscore").toString();
        settings->endGroup();
    }
    else
    {
        settings->endGroup();
        return 0;
    }

    QByteArray cipher = QByteArray::fromBase64(cipher_base64.toUtf8());

    return scipher->bArrayToInt(scipher->decrypt(cipher));
}

int MainWindow::loadAllEatApple()
{
    QString cipher_base64;

    settings->beginGroup("statistic");

    if (settings->contains("apple"))
    {
        cipher_base64 = settings->value("apple").toString();
        settings->endGroup();
    }
    else
    {
        settings->endGroup();
        return 0;
    }

    QByteArray cipher = QByteArray::fromBase64(cipher_base64.toUtf8());

    return scipher->bArrayToInt(scipher->decrypt(cipher));
}

int MainWindow::loadLosers()
{
    QString cipher_base64;

    settings->beginGroup("statistic");

    if (settings->contains("losers"))
    {
        cipher_base64 = settings->value("losers").toString();
        settings->endGroup();
    }
    else
    {
        settings->endGroup();
        return 0;
    }

    QByteArray cipher = QByteArray::fromBase64(cipher_base64.toUtf8());

    return scipher->bArrayToInt(scipher->decrypt(cipher));
}

void MainWindow::saveHighScore(int score)
{
    QByteArray cipher = scipher->encrypt(scipher->intToBArray(score));
    settings->beginGroup("statistic");
    settings->setValue("hscore", QString(cipher.toBase64()));
    settings->endGroup();
}
