#ifndef GAME_H
#define GAME_H

#include <QRandomGenerator>
#include <QPainter>
#include <QMediaPlayer>

namespace Direction
{
    const QPoint
        Up = QPoint(0, -1),
        Down = QPoint(0, 1),
        Left = QPoint(-1, 0),
        Right = QPoint(1, 0);
}

const int maxObstacle = 10;
const int maxFood = 3;
const int snakeInitSize = 3;

enum State
{
    Ready,
    Playing,
    Paused,
    GameOver
};

enum GameMode
{
    Normal,
    Hard,
    Automatic,
    Penetration,
    NotPenetration,
    Belching,
    NotBelching
};

enum RulesSituation
{
    StillAlive,
    AppleWasEaten,
    AppleWasNotEaten,
    Death
};

class Game
{

public:
    static const int fieldWidth = 50, fieldHeight = 40; // Field size in blocks
    static const int blocksMargin = 3;                  // Margins between blocks in px
    State state = State::Ready;
    GameMode gm = GameMode::Normal;
    GameMode penetration = GameMode::Penetration;
    GameMode belching = GameMode::NotBelching;

    QMediaPlayer * player = new QMediaPlayer();

    void init();                                   // Initialize all game things (food, snake, state)
    void next();                                   // Calculate next step according to the game rules
    void draw(QSize frameSize, QPainter *painter); // Draw everything
    void turn(QPoint newDirection);                // Change direction; see namespace Direction
    inline int score() const { return snake.length() - snakeInitSize; }

private:
    inline void drawBlock(float x, float y, float w, float h, QPainter *painter)
    {
        painter->drawRoundRect(
            x * w + blocksMargin, y * h + blocksMargin,
            w - blocksMargin, h - blocksMargin,
            40, 40);
    }

    void drawField(float blockWidth, float blockHeight, QPainter *painter);

    inline int _genBounded(int bound) { return QRandomGenerator::global()->bounded(0, bound); }
    bool _checkCoord(QPoint p); // Check if p overlays the food or any snake segment

    QPoint food[maxFood];
    void generateFood(int i); // i - index of the food array
    void drawFood(float blockWidth, float blockHeight, QPainter *);

    QPoint obstacle[maxObstacle];
    void generateObstacle(int i); // i - index of the obstacle array
    void drawObstacle(float blockWidth, float blockHeight, QPainter *);

    QPoint teleport[2];
    void generateTeleport(int i); // i - index of the teleport array
    void drawTeleport(float blockWidth, float blockHeight, QPainter *);

    QPoint direction = Direction::Up;
    QVector<QPoint> snake;
    void drawSnake(float blockWidth, float blockHeight, QPainter *);

    RulesSituation checkBump();
    RulesSituation checkFood();
    RulesSituation checkTeleport();
    RulesSituation checkObstacle();

    int nearestFoodX(); // X of food with Y < snake head Y
};

#endif // GAME_H
